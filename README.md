# Scrapy crawler for rogueterritory.com, sessun.com, nililotan.com

### Installation

Install Scrapy using follow official guide: https://doc.scrapy.org/en/latest/intro/install.html

### Crawler configuration

To adjust crawler settings you can either change `tbox/settings.py` file or create file '`local_settings.py` in current (Scrapy project root) directory.
Please note that `local_settings.py` will overwrite corresponding settings in `tbox/settings.py`.

Standard Scrapy settings are described here: https://doc.scrapy.org/en/latest/topics/settings.html

Settings below are used in the current project and can be customized:
    
* `LOG_LEVEL`       - Minimum level to log. Available levels are: CRITICAL, ERROR, WARNING, INFO, DEBUG *Default: INFO*

* `FEED_FORMAT`     - The serialization format to be used for the feed. *Default: csv*

* `FEED_URI`        - The URI or filesystem path of the export feed. *Default: output/%(name)s_%(time)s.csv*

* `FILES_STORE`     - The URI or filesystem path to store downloaded images. *Default: output/images*


### Running the crawler

To scrape products list and download images from certain site, run

    scrapy crawl rogueterritory.com
    
    scrapy crawl sessun.com
    
    scrapy crawl nililotan.com
    
Crawler stores output for corresponding site in the `./output_directory` in the CSV file named with site name and 
timestamp, for ex. `nililotan.com_2018-05-23T20-14-12.csv`. Crawler filters product duplicates as well. 

Images are stored in the directory `output/images`  and subdirectory named with site name, 
for ex. `output/images/nililotan.com`. File names are generated automatically and 
stored in the output feed in the field `images` as pipeline-delimited (`|`) list.