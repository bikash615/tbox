# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.loader.processors import *
from tbox.items import ProductItemLoader

__author__ = 'mikhail.kolganov@gmail.com'


class SessunComItemLoader(ProductItemLoader):
    # Prices has european-style formatting
    # (Euros and cents delimited with , rather than .)
    # so we just replace ',' with '.'
    price_out = Compose(TakeFirst(), lambda x: x.strip().replace(',', '.'))

    # Extracted value for image_urls has comma-delimited
    # list of largest-size images used for zoom view
    # We split it by ',' and returns list of URLs
    @staticmethod
    def image_urls_out(values):
        return [s.strip() for v in values for s in v.split(',')]


# This is CrawlSpider which under the hood
# visits links basing on defined `rules`
# https://doc.scrapy.org/en/latest/topics/spiders.html#crawlspider
class SessunComSpider(CrawlSpider):
    name = 'sessun.com'
    allowed_domains = ['sessun.com']
    start_urls = ['http://sessun.com/']

    # Define rules for page visiting:
    # Parse pages which URLs in last part has three or more digits.
    # Example of product URL:
    # https://boutique.sessun.com/en/dress/2330-julia-guerande
    # And follow all the other links within the site
    rules = (
        Rule(LinkExtractor(allow=r'/en/\D.+?/\d{3,}.+'), callback='parse_item'),
        Rule(LinkExtractor(allow=r'/en/'), follow=True),
    )

    def parse_item(self, response):
        # Create item loader isntance that use whole response to parse item data
        # More info about loader in rogueterritory_com.py
        l = SessunComItemLoader(response=response)
        # We use product id as unique value to filter duplicates
        # It is extracted from input parameter of form which add product to cart
        l.add_css('id', 'input[name=id_product]::attr(value)')
        # As rogueterritory.com this site has microdata markup so
        # product name tag has attribute `itemprop="name"`
        l.add_css('name', '#content h2[itemprop=name]::text')
        # The same as above, current price tag has microdata
        # attribute `itemprop="price"`
        # We use regular expression (re=) to extract only digital
        # value without currency symbol
        l.add_css('price', '#content span[itemprop=price]::text', re='\d[\d,]*')
        # Page has multiple h6 tags but section name contained in
        # one that preceding cart <form> tag
        l.add_xpath('category', '//form/preceding-sibling::h6/text()')
        # Extract comma-delimited value from the certain tag's attribute
        l.add_css('image_urls', 'button::attr(data-url)')
        return l.load_item()
