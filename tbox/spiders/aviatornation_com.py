# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor

from tbox.items import ProductItemLoader

__author__ = 'mikhail.kolganov@gmail.com'


# We have custom item loader for each site to offload data processing
# logic from spider
class AviatorNationComItemLoader(ProductItemLoader):
    @staticmethod
    def image_urls_out(links, loader_context):
        # removes query string (one after '?') and adds http schema
        # for image urls
        return [loader_context['response'].urljoin(link.split('?')[0]) for link
                in links]


class AviatorNationComSpider(scrapy.Spider):
    name = 'aviatornation.com'
    allowed_domains = ['aviatornation.com']
    start_urls = ['https://www.aviatornation.com/']
    unwanted_categories = [r'kids-collection', r'sale',
                           r'festival-collab-merch', r'aviator-nation-gift-card',
                           r'new-arrivals']
    # LinkExtractor to get section URLs from top menu
    # restrict_css has two expressions:
    # first - extracts links to subsections if section has them
    # (for ex. Demim has three:  Slim-straight, skinny and tapered)
    # so in this case we visit subsections but not section page itself
    # second - extracts links to sections that don't have
    # nested subsections (for ex. Sale)
    # `allow` mask (regular expression) ensures
    # that we extract only urls containing `/collections/` part
    lx_sections = LinkExtractor(restrict_css=(
        '.main_nav>.dropdown_container>.menu>.dropdown_content>.dropdown_column>ul>li'
    ), allow=r'/collections/', deny=unwanted_categories)
    # LinkExtractor to get product URLs from section page
    # retrieve only urls containing `/products/` part
    lx_products = LinkExtractor(
        restrict_css='.product-list>div>div.product-wrap>a.product-info__caption',
        allow=r'/products/'
    )

    def parse(self, response):
        # By default `parse` method is the first step of parsing `start_urls`
        # Here we extract sections/subsections links (see above lx_sections)
        for l in self.lx_sections.extract_links(response):
            yield scrapy.Request(l.url, self.parse_section)

    def parse_section(self, response):
        # First we extract section name
        try:
            next_page = response.css('.js-load-more a::text').extract_first()
        except Exception as e:
            next_page = ''
            print(str(e))

        section = response.css('div.breadcrumb_text span a::attr(title)').extract()[1]
        # Get all the product links in current section/subsection
        for l in self.lx_products.extract_links(response):
            # And pass category name to parse_item callback as
            # request's meta parameter
            yield scrapy.Request(l.url, self.parse_item,
                                 meta={'section': section})

        try:
            if next_page.encode('utf-8').strip() == 'Load More Products':
                next_url = response.css('.js-load-more a::attr(href)').extract_first()
                next_url = next_url.encode('utf-8').strip()
                next_url = response.urljoin(next_url)
                yield scrapy.Request(next_url, callback=self.parse_section)
        except Exception as e:
            print('No next page')

    def parse_item(self, response):
        # Create item loader isntance that use whole response to parse item data
        # To extract pieces of data loaders mostly use
        # either CSS selectors (add_css)
        # https://www.w3schools.com/cssref/css_selectors.asp
        # or XPath expressions (add_xpath) which are more difficult
        # but much more flexible than CSS selectors
        # https://www.w3schools.com/xml/xpath_intro.asp
        l = AviatorNationComItemLoader(response=response)
        # We use unique id to filter out duplicates because the same product
        # can be presented in multiple sections for rogueterritory we use
        # product id extracted from the attribute of div
        # which is 'Add to wishlist' button in html layout
        l.add_css('id', 'input[name=product_id]::attr(value)')
        # Product name is the caption of H1 tag.
        # Well-designed ecommerce sites (like Shopify)
        # mostly has semantic markup (microdata)
        # and `itemprop="name"` attribute of tag indicates
        # that tag contains product name
        # More information about microdata markup: http://schema.org/
        l.add_css('name', 'h1.product_name::text')
        # Similar as above price tag has microdata
        # markup attribute `itemprop="price"`
        l.add_css('price', 'span.current_price  span[class=money]::text')
        # Add category (section) name which we have extracted on previous step
        # and passed in `meta` attribure of response
        l.add_value('category', response.meta.get('section', ''))
        # Extract huge-size image from the img attributes
        l.add_css('image_urls',
                  'div.gallery-wrap>div:not(.product_gallery_nav)>div.gallery-cell>a>div.image__container>img::attr(data-src)')
        return l.load_item()
