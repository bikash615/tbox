# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.loader.processors import Compose, Identity
from scrapy.spiders import CrawlSpider, Rule

from tbox.items import ProductItemLoader


class NililotanComItemLoader(ProductItemLoader):
    # Get and format last item from breadcrumb as category name
    category_out = Compose(reversed, ProductItemLoader.default_output_processor)
    # Pass list of urls AS IS without further transformation
    image_urls_out = Identity()


# This is CrawlSpider which under the hood visits links basing on defined `rules`
# https://doc.scrapy.org/en/latest/topics/spiders.html#crawlspider
class NililotanComSpider(CrawlSpider):
    name = 'nililotan.com'
    allowed_domains = ['nililotan.com']
    start_urls = ['http://nililotan.com/']
    # There are a lot of links related to product comparsion and wishlist
    # so we just define url masks to ignore them and use them in `rules`
    deny_url = (r'/product_compare/', r'/wishlist/')

    # Define rules for page visiting:
    # Parse pages for urls retrived from lists of products
    # (which are defined with `restrict_css` CSS selector)
    # And follow all the other links within the site
    rules = (
        Rule(LinkExtractor(
            restrict_css='.category-products .products-grid .item',
            deny=deny_url),
            callback='parse_item'),
        Rule(LinkExtractor(deny=deny_url), follow=True),
    )

    def parse_item(self, response):
        # Create item loader isntance that use whole response to parse item data
        # More info about loader in rogueterritory_com.py
        l = NililotanComItemLoader(response=response)
        # We use product SKU as unique id to filter duplicates
        l.add_css('id', '.product-sku::text')
        # Product name is the caption of H1 tag
        l.add_css('name', '.product-name h1::text')
        # Current price (either normal or sale) contained in span
        # element with `id="product-price-XXXXXX` attribute
        # nested in div element with `class="price-box"`
        # More about XPath https://www.w3schools.com/xml/xpath_intro.asp
        # We use regular expression (re=) to extract only
        # digital value without currency symbol
        l.add_xpath('price',
                    '//div[@class="price-box"]//span[starts-with(@id, "product-price-")]//text()',
                    re=r'\$(\d[\d.,]*)')
        # Category (section) contained in breadcrumb element
        l.add_css('category', '.breadcrumbs a::text')
        # Images are organized in carousel which enclosed in
        # element with `class="product-image-gallery"
        # each image element has attribute `data-zoom-image` which
        # contains link to the huge image used for zoom view
        l.add_css('image_urls', '.product-image-gallery img::attr(data-zoom-image)')
        return l.load_item()
