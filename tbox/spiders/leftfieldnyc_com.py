# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor

from tbox.items import ProductItemLoader

__author__ = 'mikhail.kolganov@gmail.com'


# We have custom item loader for each site to offload data processing
# logic from spider
class LeftFieldNycComItemLoader(ProductItemLoader):
    @staticmethod
    def image_urls_out(links, loader_context):
        # removes query string (one after '?') and adds http schema for
        # image urls
        return [loader_context['response'].urljoin(link.split('?')[0]) for link
                in links]


class LeftFieldNycComSpider(scrapy.Spider):
    name = 'leftfieldnyc.com'
    allowed_domains = ['leftfieldnyc.com']
    start_urls = ['https://leftfieldnyc.com/']
    unwanted_categories = [r'dinerware', r'sale',
                           r'rings', r'cards', r'pine-and-tassel',
                           u'accessories', u'magazines', u'vintage-press-iron-on',
                           u'embroidery', u'seconds']
    # LinkExtractor to get section URLs from top menu
    # restrict_css has two expressions:
    # first - extracts links to subsections if section has them
    # (for ex. Demim has three:  Slim-straight, skinny and tapered)
    # so in this case we visit subsections but not section page itself
    # second - extracts links to sections that don't have nested
    # subsections (for ex. Sale)
    # `allow` mask (regular expression) ensures that we extract only
    # urls containing `/collections/` part
    lx_sections = LinkExtractor(restrict_css=(
        # '.collection-collage__item>.collection-collage__item-wrapper',
        '.drawer--left>.drawer__inner>ul>li>.mobile-nav__has-sublist',
        '.drawer--left>.drawer__inner>ul>li',
    ), allow=r'/collections/', deny=unwanted_categories)
    # LinkExtractor to get product URLs from section page
    # retrieve only urls containing `/products/` part
    lx_products = LinkExtractor(
        restrict_css='.grid-product__wrapper a.grid-product__meta',
        allow=r'/products/'
    )

    def parse(self, response):
        # By default `parse` method is the first step of parsing `start_urls`
        # Here we extract sections/subsections links (see above lx_sections)
        for l in self.lx_sections.extract_links(response):
            yield scrapy.Request(l.url, self.parse_section)

    def parse_section(self, response):
        try:
            last_span = response.css('.pagination span[class=next]')
            next_page = last_span.css('a::attr(href)').extract()
        except Exception as e:
            next_page = []
        # First we extract section name
        section = response.css('header.section-header h1::text').extract_first()
        # Get all the product links in current section/subsection
        for l in self.lx_products.extract_links(response):
            # And pass category name to parse_item callback as
            # request's meta parameter
            yield scrapy.Request(l.url, self.parse_item, meta={'section': section})

        try:
            if len(next_page) > 0:
                next_page = next_page[0]
                next_page = next_page.encode('utf-8').strip()
                next_page = response.urljoin(next_page)
                yield scrapy.Request(next_page, callback=self.parse_section)
        except Exception as e:
            print('No next page', e)

    def parse_item(self, response):
        # Create item loader isntance that use whole response to parse item data
        # To extract pieces of data loaders mostly use
        # either CSS selectors (add_css)
        # https://www.w3schools.com/cssref/css_selectors.asp
        # or XPath expressions (add_xpath) which are more difficult but
        # much more flexible than CSS selectors
        # https://www.w3schools.com/xml/xpath_intro.asp
        l = LeftFieldNycComItemLoader(response=response)
        # We use unique id to filter out duplicates because the same product
        # can be presented in multiple sections
        # for rogueterritory we use product id extracted from the attribute of div
        # which is 'Add to wishlist' button in html layout
        l.add_css('id', 'body::attr(id)')
        # Product name is the caption of H1 tag.
        # Well-designed ecommerce sites (like Shopify) mostly has semantic
        # markup (microdata) and `itemprop="name"` attribute of tag indicates
        # that tag contains product name
        # More information about microdata markup: http://schema.org/
        l.add_css('name', '.product-single__meta h1[itemprop=name]::text')
        # Similar as above price tag has microdata markup attribute
        # `itemprop="price"`
        l.add_css('price', '#ProductPrice span[class=money]::text')
        # Add category (section) name which we have extracted on previous step
        # and passed in `meta` attribure of response
        l.add_value('category', response.meta.get('section', ''))
        # Extract huge-size image from the img attributes
        l.add_css('image_urls', '.product-single__photo-wrapper img::attr(src)')
        return l.load_item()
