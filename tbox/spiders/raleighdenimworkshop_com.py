# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor

from tbox.items import ProductItemLoader

__author__ = 'mikhail.kolganov@gmail.com'


# We have custom item loader for each site to offload data
# processing logic from spider
class RaleighDenimWorkshopComItemLoader(ProductItemLoader):
    @staticmethod
    def image_urls_out(links, loader_context):
        # removes query string (one after '?') and
        # adds http schema for image urls
        return [loader_context['response'].urljoin(link.split('?')[0]) for link in links]


class RaleighDenimWorkshopComSpider(scrapy.Spider):
    name = 'raleighdenimworkshop.com'
    allowed_domains = ['raleighdenimworkshop.com']
    start_urls = ['https://www.raleighdenimworkshop.com/']
    unwanted_category = [r'all-womens']
    # LinkExtractor to get section URLs from top menu
    # restrict_css has two expressions:
    # first - extracts links to subsections if section has them
    # (for ex. Demim has three:  Slim-straight, skinny and tapered)
    # so in this case we visit subsections but not section page itself
    # second - extracts links to sections that don't have nested
    # subsections (for ex. Sale)
    # `allow` mask (regular expression) ensures that we extract
    # only urls containing `/collections/` part
    lx_sections = LinkExtractor(restrict_css=(
        '.nav__main-menu>div:not(.xlarge--hide)>ul>li>.site-nav__dropdown>div.inner>ul>li>.site-nav__link',
    ), allow=r'/collections/', deny=unwanted_category)
    # LinkExtractor to get product URLs from section page
    # retrieve only urls containing `/products/` part
    lx_products = LinkExtractor(
        restrict_css='.product-item .inner a.grid__image', allow=r'/products/')

    def parse(self, response):
        # By default `parse` method is the first step of parsing `start_urls`
        # Here we extract sections/subsections links (see above lx_sections)
        for l in self.lx_sections.extract_links(response):
            yield scrapy.Request(l.url, self.parse_section)

    def parse_section(self, response):
        # First we extract section name
        section = response.css('main.main-content .wrapper span.mobile-title::text').extract_first()
        # Get all the product links in current section/subsection
        for l in self.lx_products.extract_links(response):
            # And pass category name to parse_item callback as request's
            # meta parameter
            yield scrapy.Request(
                l.url,
                self.parse_item,
                meta={'section': section}
            )

    def parse_item(self, response):
        # Create item loader isntance that use whole response to parse item data
        # To extract pieces of data loaders mostly use
        # either CSS selectors (add_css)
        # https://www.w3schools.com/cssref/css_selectors.asp
        # or XPath expressions (add_xpath) which are more difficult
        # but much more flexible than CSS selectors
        # https://www.w3schools.com/xml/xpath_intro.asp
        l = RaleighDenimWorkshopComItemLoader(response=response)
        # We use unique id to filter out duplicates because the same
        # product can be presented in multiple sections
        # for rogueterritory we use product id extracted from the attribute of div
        # which is 'Add to wishlist' button in html layout
        l.add_css('id', 'body::attr(id)')
        # Product name is the caption of H1 tag.
        # Well-designed ecommerce sites (like Shopify) mostly
        # has semantic markup (microdata)
        # and `itemprop="name"` attribute of tag indicates that
        # tag contains product name
        # More information about microdata markup: http://schema.org/
        l.add_css('name', 'h1[class=product-title]::text')
        # Similar as above price tag has
        # microdata markup attribute `itemprop="price"`
        l.add_css('price', 'span[id=ProductPrice]::text')
        # Add category (section) name which we have extracted on previous step
        # and passed in `meta` attribure of response
        l.add_value('category', response.meta.get('section', ''))
        # Extract huge-size image from the img attributes
        l.add_css('image_urls',
                  'div.product-photos-container>.main-image-container>.product-single__photos>div::attr(data-zoom)')
        return l.load_item()
