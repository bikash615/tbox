# -*- coding: utf-8 -*-
import os.path
from scrapy.exceptions import DropItem
from scrapy.pipelines.files import FilesPipeline


class TboxImagePipeline(FilesPipeline):
    """
    Modified FilesPipeline that stores data in site subdirectories
    and returns pipeline-delimited list of file paths
    """
    DELIMITER = '|'

    def item_completed(self, results, item, info):
        if isinstance(item, dict) or self.files_result_field in item.fields:
            item[self.files_result_field] = self.DELIMITER.join([x['path'] for ok, x in results if ok])
        return item

    def file_path(self, request, response=None, info=None):
        file_path = super(TboxImagePipeline, self).file_path(request, response, info)
        return os.path.join(info.spider.name, file_path[file_path.index('/') + 1:])


class TBoxUniqueFilterPipeline(object):
    """
    Filters duplicates using id field
    """
    seen = set()

    def process_item(self, item, spider):
        # If item has `id` attribute we check if it is already in `seen` list
        # and either drop it if already seen
        # or proceed and add its id to the `seen` list
        if item.get('id', None):
            if item['id'] in self.seen:
                raise DropItem()
            self.seen.add(item['id'])
        return item
