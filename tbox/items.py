# -*- coding: utf-8 -*-
import six
import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import *

__author__ = 'mikhail.kolganov@gmail.com'


# Items documentation: https://doc.scrapy.org/en/latest/topics/items.html
class ProductItem(scrapy.Item):
    """
    id          - unique product id, used for duplicate filtering
    name        - product name
    price       - product normal price or sale price if available
    category    - site's section name where product appeared
    image_urls  - list of image source URLs (not used in output data)
    images      - pipeline(|)-delimited list of filenames for product images which are stored locally
    image_url_xx - xx from 1 to 32, image_urls
    """
    id = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    category = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()
    image_url_1 = scrapy.Field()
    image_url_2 = scrapy.Field()
    image_url_3 = scrapy.Field()
    image_url_4 = scrapy.Field()
    image_url_5 = scrapy.Field()
    image_url_6 = scrapy.Field()
    image_url_7 = scrapy.Field()
    image_url_8 = scrapy.Field()
    image_url_9 = scrapy.Field()
    image_url_10 = scrapy.Field()
    image_url_11 = scrapy.Field()
    image_url_12 = scrapy.Field()
    image_url_13 = scrapy.Field()
    image_url_14 = scrapy.Field()
    image_url_15 = scrapy.Field()
    image_url_16 = scrapy.Field()
    image_url_17 = scrapy.Field()
    image_url_18 = scrapy.Field()
    image_url_19 = scrapy.Field()
    image_url_20 = scrapy.Field()
    image_url_21 = scrapy.Field()
    image_url_22 = scrapy.Field()
    image_url_23 = scrapy.Field()
    image_url_24 = scrapy.Field()
    image_url_25 = scrapy.Field()
    image_url_26 = scrapy.Field()
    image_url_27 = scrapy.Field()
    image_url_28 = scrapy.Field()
    image_url_29 = scrapy.Field()
    image_url_30 = scrapy.Field()
    image_url_31 = scrapy.Field()
    image_url_32 = scrapy.Field()


# Item Loaders documentation: https://doc.scrapy.org/en/latest/topics/loaders.html
# Concept of item loader is very helpful as it allows to separate parsing and data processing
class ProductItemLoader(ItemLoader):
    default_item_class = ProductItem
    default_output_processor = Compose(TakeFirst(), six.text_type, six.text_type.strip)

    def load_item(self, *args, **kwargs):
        item = super(ProductItemLoader, self).load_item(*args, **kwargs)
        # Populate image_url_xx columns with one url per column
        for i, u in enumerate(item.get('image_urls', [])[:32], 1):
            item['image_url_%s' % i] = u
        return item
